package com.miguelesteban.despensapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.miguelesteban.despensapp.R.layout.activity_recetas

class MainActivity : AppCompatActivity() {
    internal lateinit var recestasButton:Button
    internal lateinit var listasButton:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recestasButton = findViewById(R.id.buttonRecetas)
        listasButton = findViewById(R.id.buttonListas)

        recestasButton.setOnClickListener {
            val intent = Intent(this, RecetasActivity::class.java)
            startActivity(intent)
        }

        listasButton.setOnClickListener {
            val intent = Intent(this, ListasActivity::class.java)
            startActivity(intent)
        }
    }
}
